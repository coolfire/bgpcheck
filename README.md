# BGP route check
Uses the ripe looking-glass API to check if a specified IP address is being routed by any unexpected ASN. Built as a very rudimentary check for BGP hijacking.
