# frozen_string_literal: true

# /usr/bin/env ruby

require 'net/http'
require 'yaml'

# Load config and prepare variables
config = YAML.load_file 'config.yaml'
uri    = URI("#{config['url']}#{config['ip']}")
data   = nil
result = {
  'ok' => true,
  'rrcs' => {}
}

# Make HTTP API request and parse response
Net::HTTP.start(uri.host, uri.port, use_ssl: true) do |http|
  request  = Net::HTTP::Get.new uri
  response = http.request request
  data     = YAML.load response.body
end

# Output extra info
if config['verbose']
  puts "Request status : #{data['status']}"
  puts "Cached result  : #{data['cached']}"
  puts "Result time    : #{data['data']['latest_time']}\n"
end

# Check for unexpected routes and ASN origins
data['data']['rrcs'].each do |rrc|
  result['rrcs'][rrc['rrc']]              = {}
  result['rrcs'][rrc['rrc']]['location']  = rrc['location']
  result['rrcs'][rrc['rrc']]['badasns']   = []
  result['rrcs'][rrc['rrc']]['badroutes'] = []

  rrc['peers'].each do |peer|
    unless config['asn'].include? peer['asn_origin']
      result['ok'] = false
      result['rrcs'][rrc['rrc']]['badasns'].push "  ASN origin: #{peer['asn_origin']}"
    end

    unless config['asn'].include? peer['as_path'].split.last.to_i
      result['ok'] = false
      result['rrcs'][rrc['rrc']]['badroutes'].push "  AS path: #{peer['as_path']}"
    end
  end
end

# Display results
if result['ok']
  puts 'OK'
else
  puts 'UNEXPECTED RESULTS FOUND!'
end

if result['ok'] == false || config['verbose']
  result['rrcs'].each do |rrc, data|
    print "#{rrc} (#{data['location']}): "
    if data['badasns'].empty? && data['badroutes'].empty?
      puts 'OK'
    else
      puts 'FAIL'
      puts data['badasns'].uniq
      puts data['badroutes'].uniq
    end
  end
end
